**Instructions for Building and Testing the Application**

- Run git clone "repository url" in the command line.
- Run the command "yarn" to get the node modules.
- Run the command "yarn start" to build and run the app
- Now you can see the app in browser.

---

**Sections done**

- Article Hero with text and other information on top of image
- Article Summary text that appears directly under Hero
- Text block
- Image Set block

**Comments about the work done**

- Done all the four sections you have mentioned in the document.
- Couldn't able to add all the different image url's for various devices due to shortage of time.
- Other style perfections and code refactoring can be done with more time.

---