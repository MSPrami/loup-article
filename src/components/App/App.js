import React from 'react';
import Header from '../Header/Header';
import Article from '../Article/Article';

import './App.scss';
import './font.scss';

function App () {
  return(
     <div>
        <Header />
        <Article />
     </div>
  );
}
export default App;