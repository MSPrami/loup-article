import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container } from 'react-bootstrap';

import { getArticle } from './ArticleSelectors';
import { loadArticleMockThunk } from './ArticleActions';
import HeroImageBlock from './components/HeroImageBlock/HeroImageBlock';
import TextBlock from '../TextBlock/TextBlock';
import TextImageBlock from '../TextImageBlock/TextImageBlock';

import './Article.scss';

function renderBlocks(block) {
  if (block.blockTypeId === 0) {
    return <TextBlock key={block.blockTypeId} content={block.content} />;
  }
  return <TextImageBlock  key={block.blockTypeId} content={block.content} />;
}

class Article extends Component {

  componentDidMount() {
    this.props.loadArticle();
  }
  
  render() {
    const { article } = this.props;
    if (article) {
      return (
        <div className="article">
          <HeroImageBlock
            image={article.imageList.landscapedesktop2x.url}
            title={article.title}
            introText={article.introText}
            author={article.authors[0]}
          />
           <Container>
            <div className="articleSummary"><TextBlock content={article.summary} /></div>
            <hr />
            {
              article.blocks.map(block => (
                renderBlocks(block)
              ))
            }
          </Container>
        </div>
      );
    }
    return null;
  }
}

Article.defaultProps = {
  article: null,
};

Article.propTypes = {
  loadArticle: PropTypes.func.isRequired,
  article: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    article: getArticle(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadArticle : () => dispatch(loadArticleMockThunk()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);
