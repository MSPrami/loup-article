import {
  loadArticleApiMock,
} from '../../services/article.api';

export const REQUEST_ARTICLE = 'REQUEST_ARTICLE';
export const ARTICLE_LOAD_SUCCESSFUL = 'ARTICLE_LOAD_SUCCESSFUL';
export const ARTICLE_LOAD_FAILED = 'ARTICLE_LOAD_FAILED';

export function requestArticle() {
  return {
    type: REQUEST_ARTICLE,
  };
}

export function articleLoadSuccessful(articleData) {
  return {
    type: ARTICLE_LOAD_SUCCESSFUL,
    articleData,
  };
}

export function articleLoadFailed(error) {
  return {
    type: ARTICLE_LOAD_FAILED,
    error,
  };
}

// Thunk

export function loadArticleMockThunk() {
  return (dispatch) => {
    dispatch(requestArticle());
    loadArticleApiMock()
      .then((result) => {
        if (result.error) {
          dispatch(articleLoadFailed(new Error(result.error)));
        } else {
          dispatch(articleLoadSuccessful(result.article));
        }
      }, (error) => {
        dispatch(articleLoadFailed(error));
      });
  };
}