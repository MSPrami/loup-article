import {
  REQUEST_ARTICLE,
  ARTICLE_LOAD_SUCCESSFUL,
  ARTICLE_LOAD_FAILED,
} from './ArticleActions';

const initialState = {
  article: null,
  error: null,
};

function loadArticleSuccess(state, action) {
  return {
    ...state,
    article: action.articleData,
  };
}

export default function ProjectReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_ARTICLE:
      return state;
    case ARTICLE_LOAD_SUCCESSFUL:
        return loadArticleSuccess(state, action);
    case ARTICLE_LOAD_FAILED:
      return {
        error: action.error,
      };
    default:
      return state;
  }
}
