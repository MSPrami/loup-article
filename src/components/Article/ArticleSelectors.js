import { createSelector } from 'reselect';

const getArticleSelector = state => state.article.article;

export const getArticle = createSelector(
  [getArticleSelector],
  state => state,
);
