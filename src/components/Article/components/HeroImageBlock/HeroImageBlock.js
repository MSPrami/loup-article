import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import './HeroImageBlock.scss';

const HeroImageBlock = ({
  image,
  title,
  introText,
  author
}) => {
  return (
    <div className="heroImageBlock" style={{ backgroundImage: `url('${image}')` }}>
      <div className="darkOverlay" />
      <div className="content">
        <h4 className="introText">
        {introText}
        </h4>
        <h2 className="title">
          {title}
        </h2>
          <Row className="sectionSubHeading">
            <Col xs={3} md={5}>
              <img className="rounded-circle" src={author.imageList.square1x.url} alt="authorImage" />
            </Col>
            <Col xs={9} md={7} className="verticleAlignCenter">
              <p className="caption">{author.name}</p>
            </Col>
          </Row>
        </div>
    </div>
  );
}

HeroImageBlock.defaultProps = {
  introText: null,
  author: null,
};

HeroImageBlock.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  introText: PropTypes.string,
  author: PropTypes.object,
};

export default HeroImageBlock;