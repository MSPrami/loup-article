import React from 'react';
import { Row, Col } from 'react-bootstrap';
import hemsworthLogo from '../../assets/images/hemsworthLogo.png';
import burgerIcon from '../../assets/images/burger-icon@3x.png';

export default function Header() {
  return (
    <Row className="header">
      <Col xs={4}>
        <p>ARTICLE</p>
      </Col>
      <Col xs={4} className="text-center">
        <img src={hemsworthLogo} alt="logo" />
      </Col>
      <Col xs={4} className="text-right">
        <img src={burgerIcon} alt="logo" />
      </Col>
    </Row>
  );
}
