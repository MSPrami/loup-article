import React from 'react';
import PropTypes from 'prop-types';

export const ICONS = {
  nextArrow: 'M6.22111621,0 L5,1.1945702 L14.6162897,11.003318 C8.20542991,17.5469084 5,20.8187036 5,20.8187036 C5,20.8187036 5.40703874,21.2124691 6.22111621,22 L16.992158,11.003318 L6.22111621,0 Z',
  prevArrow: 'M15.7710418,0 L5,11.003318 C12.1806945,18.3344393 15.7710418,22 15.7710418,22 C15.7710418,22 16.1780805,21.6062345 16.992158,20.8187036 L7.37586826,11.003318 L16.992158,1.1945702 L15.7710418,0 Z',
};

const Icon = ({ icon, size, colour }) => (
  <svg className="icon" width={`${size}px`} height={`${size}px`} viewBox="0 0 22 22">
    <path d={icon} fill={colour} />
  </svg>
);

Icon.defaultProps = {
  size: 22,
  colour: '#000',
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  size: PropTypes.number,
  colour: PropTypes.string,
};

export default Icon;
