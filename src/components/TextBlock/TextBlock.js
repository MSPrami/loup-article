import React from 'react';
import PropTypes from 'prop-types';

const TextBlock = ({
  heading,
  content
}) => {
  return (
    <div className="content sectionHeading">
    {
      heading &&
        <h4>
          {heading}
        </h4>
    }
      <p>{content}</p>
    </div>
  );
}

TextBlock.defaultProps = {
  heading: null,
};

TextBlock.propTypes = {
  content: PropTypes.string.isRequired,
  heading: PropTypes.string,
};

export default TextBlock;