import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Icon, { ICONS } from '../Icon/Icon';
import './TextImageBlock.scss';

const TextImageBlock = ({
  heading,
  content
}) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const maxContentLength = content.length;

  const onPreviousSlide = () => {
    if (currentSlide >= 0)
    setCurrentSlide(currentSlide - 1); 
  }
  const onNextSlide = () => {
    if (currentSlide < maxContentLength )
    setCurrentSlide(currentSlide + 1);
  }
  return (
    <div className="content sectionHeading">
      {
        heading &&
          <h4>
            {heading}
          </h4>
      }

      <div className="carousalSlide">
        <div className="imageBlock">
          <picture>
            <source media="(min-width: 650px)" srcSet={content[currentSlide].imageList.landscapedesktop1x.url} />
            <source media="(min-width: 465px)" srcSet={content[currentSlide].imageList.landscapemobile1x.url} />
            <img src={content[currentSlide].imageList.landscapedesktop1x.url} alt="" />
          </picture>
          {
            currentSlide !== 0 && (
              <button className="previous" type="button" onClick={onPreviousSlide}>
                <Icon icon={ICONS.prevArrow} size={40} />
              </button>)
          }
          {
            currentSlide < maxContentLength - 1 && (
              <button className="next" type="button" onClick={onNextSlide}>
                <Icon icon={ICONS.nextArrow} size={50} />
              </button>)
          }
        </div>
        <div className="sectionSubHeading">
          <h4>{content[currentSlide].title}</h4>
          <p className="sectionSubHeading">{content[currentSlide].summary}</p>
        </div>
      </div>
      
    </div>
  );
}

TextImageBlock.defaultProps = {
  heading: null,
};

TextImageBlock.propTypes = {
  content: PropTypes.array.isRequired,
  heading: PropTypes.string,
};

export default TextImageBlock;