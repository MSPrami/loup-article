import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App/App';
import configureStore from './reducers';

const store = configureStore();

function render(Component) {
  ReactDOM.render(
    <Provider store={store}><Component /></Provider>,
    document.getElementById('root'),
  );
}

render(App);