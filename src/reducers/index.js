import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { logger } from 'redux-logger'

import ArticleReducer from '../components/Article/ArticleReducers';


const reducer = combineReducers({
  article: ArticleReducer,
});

export default function configureStore(initialState) {
  return createStore(
    reducer,
    initialState,
    applyMiddleware(
      thunkMiddleware,
      logger,
    ),
  );
}
